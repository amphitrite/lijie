# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/7 上午 10:46
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c11s1_面对对象编程概念.py
# @Software : PyCharm

# 1.什么是面对对象编程OOP
# 一切皆对象
# 将程序所有的数据及相关操作看做一个‘对象’来看待

# 2.什么是对象和类
# 对象object是个体，具有基本的：属性和方法
# 属性：描述静态特征
# 方法：描述动态特征

# 类class是对象的集合，是对象的抽象
# 类也具有属性和方法

# 3.面对对象编程的三大原则：封装、继承、多态
# 封装：将属性和方法封装为一个整体，对外隐藏类的细节
# 继承：重用现有的类生成新的类。被继承的类：父类、超类、基类，继承了其它类的:子类
# 多态：同一个父类的不同子类，以不同方式去实现父类的同一个方法（覆盖override，超载overload）
