# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/7 上午 11:50
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c11s2_面向对象程序设计.py
# @Software : PyCharm

# 对象是类的实例，是类的个体
# 类是对象的抽象，是对象的模板
# 先定义模板-类，再根据类生成对象

# 定义类
class Dog:
    # 属性
    # 类变量
    class_name = '犬'

    # 方法
    # 构造方法：
    # 1.会在生成对象时自动调用，完成对象属性的初始化；
    # 2.名字是固定__init__;
    # 3.类中必有构造方法，不写会自动化加一个空的构造方法
    def __init__(self, name, color):
        # 对象变量：使用self.定义，作用在整个对象的所有方法中
        # self:代指当前对象
        self.name = name
        self.color = color

    # 实例方法：第一个参数是self
    def eat(self, food):
        print(f'{self.name}正在吃{food}')  # food：局部变量

    def bark(self):
        print('旺旺旺')

    # 类方法:只能访问类变量，不能访问对象变量
    @classmethod
    def getClassName(cls):
        print(cls.class_name)


# 根据类生成对象
xiaomaodog = Dog('大黄', '黄色')  # 类名()：调用构造方法
# 调用对象的属性和方法
print(xiaomaodog.name)
print(xiaomaodog.color)
xiaomaodog.bark()
xiaomaodog.eat('狗粮')

xiaobaidog = Dog('小白', '黑色')
print(xiaobaidog.name)
print(xiaobaidog.color)
xiaobaidog.eat('骨头')


# 创建一个名为User 的类，其中包含属性first_name 和last_name 。
# 在类User 中定义一个名 为describe_user() 的方法，它打印用户的first_name和 last_name；
# 再定义一个名为greet_user() 的方法，它向用户发出个性化的问候（hello +first_name+last_name）。
# 创建多个表示不同用户的实例，并对每个实例都调用上述两个方法
class User():
    def __init__(self, last_name, first_name):
        self.last_name = last_name
        self.first_name = first_name
        self.__money = 10000

    def describe_user(self):
        print(f'我是{self.last_name}{self.first_name}')

    def greet_user(self):
        print(f'hello,{self.last_name}{self.first_name}')

    def lend(self, money):
        self.__money = self.__money - money
        print(f'借给你{money}元')


user1 = User('司马', '南')
print(user1.last_name)
print(user1.first_name)
user1.describe_user()
user1.greet_user()
user1.lend(1000)


# 继承
class Husky(Dog):
    def destroy(self):
        print(f'{self.name}正在拆迁')


husky1 = Husky('二哈', '黑白')
husky1.eat('肉')
husky1.bark()
husky1.destroy()


class ChinaGardenDog(Dog):
    def eat(self):
        print('啥都吃')


class CorgiDog(Dog):
    def eat(self):
        print('吃精品狗粮')


dog1 = ChinaGardenDog('旺财', '黄色')
dog2 = CorgiDog('达芬奇', '花色')
dog1.eat()
dog2.eat()
