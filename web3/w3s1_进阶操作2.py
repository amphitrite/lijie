# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/13 上午 10:36
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w.py
# @Software : PyCharm

from selenium.webdriver.common.keys import Keys  # 定义键盘的特殊按键类
from selenium.webdriver.common.by import By
from selenium import webdriver
import time


# 翻页操作
def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("http://news.baidu.com/")

    # 翻页
    for i in range(5):
        driver.find_element(By.XPATH, '/html').send_keys(Keys.PAGE_DOWN)

    for i in range(10):
        driver.find_element(By.XPATH, '/html').send_keys(Keys.ARROW_UP)
        time.sleep(0.5)

    driver.find_element(By.XPATH, '/html').send_keys(Keys.HOME)
    time.sleep(3)

    driver.quit()


# 模拟同时按下多个键
def foo2():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("http://news.baidu.com/")

    driver.find_element(By.XPATH, '/html').send_keys(Keys.CONTROL, 'a')  # 全选页面

    time.sleep(3)

    driver.quit()


# 截图
def foo3():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("http://news.baidu.com/")

    # 截图：支持png图片
    driver.get_screenshot_as_file("./screen1.png")  # 截取整个网页图片

    driver.find_element(By.XPATH, '//*[@id="channel-all"]/div').screenshot('element.png')  # 截取单个元素图片

    driver.quit()


if __name__ == "__main__":
    # foo1()
    # foo2()
    foo3()
