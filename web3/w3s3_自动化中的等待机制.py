# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/13 下午 04:10
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w3s3_自动化中的等待机制.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time


# 为什么需要等待？哪些情况下需要等待？
# 同步页面显示、页面加载和自动化的操作，保证自动化操作的成功率
# 页面加载、页面刷新、菜单弹出时需要等待

# 自动化测试中的等待机制：1.强制等待；2.隐式等待；3.显式等待
# 1.强制等待
# time.sleep(时长)
# 缺点：等待时间没有弹性，只用于调试和演示
# 优点：写法简单


# 2.隐式等待
# driver.implicitly_wait(最大等待时间)
# 等待整个页面加载设置最大等待时间
# 缺点：不是等待某个元素，而是等待页面的所有元素加载，所以等待时间可能较长
# 优点：只用写一行在整个driver生命周期都有效
def foo1():
    driver = webdriver.Chrome()
    # 设置隐式等待
    driver.implicitly_wait(30)  # 最大等待时间为30秒

    driver.maximize_window()
    driver.get('https://www.baidu.com/')

    driver.find_element(By.CLASS_NAME, 'soutu-btn').click()  # 点击搜索图片图标

    driver.find_element(By.CLASS_NAME, 'upload-pic').send_keys(r'C:\Users\LiJie\Pictures\2.jpg')  # 上传图片

    driver.quit()


# 显示等待（最推荐的等待方式）
# 等待某个具体元素，并以元素的存在或可见作为等待条件
# 优点： 灵活，准确
# 缺点：写法复杂
from selenium.webdriver.support.wait import WebDriverWait  # 显示等待类
from selenium.webdriver.support import expected_conditions  # 等待条件


def foo2():
    driver = webdriver.Chrome()
    # 生成显示等待对象
    # 参数:1.driver；2.最大等待时间
    wait = WebDriverWait(driver, 30)

    driver.maximize_window()
    driver.get('https://www.baidu.com/')

    # 设置显示等待:定位元素可见
    wait.until(expected_conditions.visibility_of_element_located((By.CLASS_NAME, 'soutu-btn')))  # 多打一层()
    driver.find_element(By.CLASS_NAME, 'soutu-btn').click()  # 点击搜索图片图标

    # 设置显示等待:定位元素的存在
    wait.until(expected_conditions.presence_of_element_located((By.CLASS_NAME, 'upload-pic')))
    driver.find_element(By.CLASS_NAME, 'upload-pic').send_keys(r'C:\Users\LiJie\Pictures\2.jpg')  # 上传图片

    driver.quit()


if __name__ == "__main__":
    # foo1()
    foo2()
