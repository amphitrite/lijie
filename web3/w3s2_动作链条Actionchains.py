# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/13 上午 11:33
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w3s2_动作链条Actionchains.py
# @Software : PyCharm

# ActionChains主要实现鼠标操作,并且可以将多个动作拼接为一个链条
from selenium.webdriver.common.action_chains import ActionChains  # 导入动作链条类
from selenium.webdriver.common.by import By
from selenium import webdriver
import time


def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get('https://map.baidu.com/@13373106,3518972,15z')

    # 点击鼠标右键
    # 参数：element=None,如果传元素，会右键点击这个元素；如果不传参数，右键点击鼠标当前位置
    ActionChains(driver).context_click().perform()  # 最后一定要加.perform()才会
    time.sleep(1)

    # 鼠标左键双击
    # 参数：element=None,如果传元素，会双击这个元素；如果不传参数，双击鼠标当前位置
    ActionChains(driver).double_click().perform()
    time.sleep(1)

    # 按住鼠标左键不放
    # 参数：element=None,如果传元素，会点击这个元素不放；如果不传参数，点击当前位置不放
    ActionChains(driver).click_and_hold().perform()
    time.sleep(1)

    # 移动鼠标
    # 参数：xoffset：x轴移动距离；yoffset：y轴移动距离
    ActionChains(driver).move_by_offset(400, 400).perform()
    time.sleep(1)

    # 释放鼠标
    ActionChains(driver).release().perform()
    time.sleep(1)

    # 拖拽
    # 参数：source：源目标元素；xoffset：x轴移动距离；yoffset：y轴移动距离
    ActionChains(driver).drag_and_drop_by_offset(None, -400, -400).perform()
    time.sleep(1)

    # 鼠标左键单击
    # ActionChains(driver).click(driver.find_element(By.XPATH, '//*[@id="map-operate"]/div[2]/button')).perform()
    # time.sleep(3)

    # 构建动作链条
    # pause(时间)：加入动作链条，暂停一定时间
    ActionChains(driver).click_and_hold().pause(1).move_by_offset(400, 400).pause(1).release().perform()

    driver.quit()


if __name__ == "__main__":
    foo1()
