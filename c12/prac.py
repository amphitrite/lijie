# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/26 下午 02:17
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1. 推算几天后的日期
# 编写一个程序，输入开始提起和间隔天数，推算出结束日期
import time


def foo1():
    start_date = input('请输入开始日期(如：20210126，按enter完成):')
    delay = input('请输入间隔天数(如：120，按enter完成):')
    start_time = time.mktime(time.strptime(start_date, '%Y%m%d'))
    end_time = start_time + int(delay) * 24 * 60 * 60
    end_date = time.strftime('%Y%m%d', time.localtime(end_time))
    print(end_date)


import datetime


def foo2():
    start_date = input('请输入开始日期(如：20210126，按enter完成):')
    delay = input('请输入间隔天数(如：120，按enter完成):')
    end_date = datetime.datetime.strptime(start_date, '%Y%m%d') + datetime.timedelta(
        days=int(delay))
    print(end_date)


# 2. 输出福彩3D号码
# 3D彩票是一个以3位自然数为投注号码的彩票。投注者从000-999范围内选择一个3位数投注
# 写一个程序完成如下功能：
# 1.随机产生一个3D投注号码
# 2.小明每期都买6个号码，输入3个固定号码外程序帮他随机产生3个投注号码，统一输出
import random


def foo3():
    lottery_numbers = []
    for i in range(3):
        lottery_number = input('请输入一个000-999的彩票号码:')
        lottery_numbers.append(lottery_number)

    for i in range(3):
        lottery_number = random.randint(0, 999)
        lottery_number = f'{lottery_number:03d}'
        lottery_numbers.append(lottery_number)

    print(f'您的彩票号码是：{lottery_numbers}')

    # win_number = '123'
    win_number = str(random.randint(0,9))+str(random.randint(0,9))+str(random.randint(0,9))
    win_team1 = 0
    win_team3 = 0
    win_team6 = 0

    for lottery_number in lottery_numbers:
        if lottery_number == win_number:
            win_team1 += 1
        elif lottery_number == win_number[2] + win_number[0:2] or lottery_number == win_number[
            0] + win_number[2] + win_number[1]:
            win_team3 += 1
        elif lottery_number == win_number[1] + win_number[0] + win_number[2] or \
                lottery_number == win_number[1] + win_number[2] + win_number[0] or lottery_number == \
                win_number[2] + win_number[1] + win_number[0]:
            win_team6+=1
        else:
            pass
    win_money = win_team1 * 1040 + win_team3 * 374 + win_team6 * 173
    print(f'大奖号码是：{win_number}')
    print(f'你中了直选：{win_team1}个，组3：{win_team3}个，组6：{win_team6}个，共计{win_money}元')


if __name__ == '__main__':
    # foo1()
    # foo2()
    foo3()
