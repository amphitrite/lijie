# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/8 上午 11:22
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c12s3_标准模块.py
# @Software : PyCharm

"""
time：时间处理相关的模块
"""
import sys
import time

print(time.time())  # 从1970年1月1号0：0：0到当前的时间经过的秒数
print(time.gmtime())  # 将当前时间转换为结构化时间UTC
print(time.localtime())  # 将当前时间转换为结构化时间(本地时区)
print(time.ctime())
strftime = time.strftime('%Y-%m-%d:%H-%M-%S')  # 指定时间格式
print(strftime)

time.sleep(1)  # 线程休眠

"""
random:随机数模块
"""
import random

print(random.random())  # 产生[0.0,1.0)的随机浮点数
print(random.randint(1, 10))  # 产生[a,b]的随机整数
list1 = ['a', 'b', 'c']
print(random.choice(list1))  # 随机选择序列中一个元素
list2 = [1, 2, 3, 4, 5]
random.shuffle(list2)  # 随机打乱序列
print(list2)

print(
    """
    math:与数学有关的模块
    """)

import math

number = 3.1415
print(math.ceil(number))  # 向上取整
print(math.floor(number))  # 向下取整
print(math.fabs(-3.1415))  # 取绝对值
print(math.factorial(4))  # 计算阶乘
print(math.pi)  # π常数
print(math.sin(90))

"""
sys:与系统相关的操作
"""
print(sys.argv)
print(sys.path)  # 获取PYTHONPATH环境变量配置
print(sys.getdefaultencoding())  # 获取系统编码
print(sys.getfilesystemencoding())  # 获取文件系统编码

