# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/8 上午 10:35
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c12s1_自定义模块.py
# @Software : PyCharm

# 自定义模块：编写一个py文件，包含函数或者类
# 自定义模块的命名：
# 1.不能用关键字，不能用标准模块名

# 模块的导入
# import 模块名 [as 别名]
"""
1.import导入的模块，每次使用都要加上模块名：模块名.函数名()
2.可以给模块命别名，例如:import random as r
3.import可以同时导入多个模块，以逗号分隔：import random,time
"""
import random

print(random.randint(1, 10))

# from 模块 import 函数名
from random import randint

print(randint(1, 10))

# 定义模块的导入
"""
1.将自定义模块复制到python\lib目录下，可以直接import或from-import
2.将自定义模块复制到项目文件夹下，导入模块前要加入项目的路径前缀
"""
import c10.c10s1_基本文件操作
c10.c10s1_基本文件操作.foo3()


from c10.c10s1_基本文件操作 import foo3
foo3()
