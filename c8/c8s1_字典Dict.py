# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/3 下午 01:52
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c8s1_字典Dict.py
# @Software : PyCharm

# 1.字典的定义
dict1 = {
    '姓名': "姚明",
    '身高': 2.26,
    '体重': 140
}

dict2 = {
    '姚明': {
        '身高': 2.26,
        '体重': 140
    },
    '詹姆斯': {
        '身高': 2.05,
        '体重': 120
    }
}

print(dict1['姓名'])
print(dict2['姚明']['身高'])

# 字典的增、删、改、查
# 新增键值对：对不存在的键赋值
dict1['年龄'] = 41
print(dict1)

# 修改键值对：对已存在的键赋值
dict1['身高'] = 1.75
print(dict1)

# 删除键值对：pop(key)
dict1.pop('身高')
print(dict1)

# 查询
for key in dict1:
    print(key)
    print(dict1[key])

# 获取所有key的列表
keys = dict1.keys()
print(keys)

# 获取所有value的列表
values = dict1.values()
print(values)
