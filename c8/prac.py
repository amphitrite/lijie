# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/21 上午 11:41
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1.模拟手机通讯录，模拟手机通讯录的增删改查操作，联系人信息需要包括姓名和电话即可
address_dict = {
    '张三': '13900000001',
    '李四': '13900000002',
    '王麻子': '13900000003'
}

choice = input('请选择通讯录功能(1.添加联系人；2.删除联系人；3查找联系人)：')
names = address_dict.keys()
if choice == '1':
    name = input('请输入联系人姓名：')
    tel = input('请输入联系人电话：')
    if name not in names:
        address_dict[name] = tel
    else:
        print(f'联系人已存在：{name}')
elif choice == '2':
    name = input('请输入联系人姓名：')
    tel = input('请输入联系人电话：')
    if name in names:
        address_dict.pop(name)
    else:
        print(f'联系人不存在：{name}')
elif choice == '3':
    name = input('请输入要查询的联系人姓名：')
    if name in names:
        print(f'联系人姓名：{name} 电话：{address_dict[name]}')
    else:
        print(f'您输入的联系人不存在：{name}')
else:
    print(f'无效的功能选项：{choice}')
