# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 11:53
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c4s2_比较运算符.py
# @Software : PyCharm

# 运算的是数值和字符串，得到布尔值
print(3 == 5)
print(3 != 5)
x = 66
print(0 < x < 100)
