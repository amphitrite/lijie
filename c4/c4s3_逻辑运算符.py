# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 01:44
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c4s3_逻辑运算符.py
# @Software : PyCharm

#逻辑运算符，运算布尔值得到布尔值
a = True
b = False
print(a and b)
print(a or b)
print(not a)