# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/13 下午 03:56
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1. 将4.3 逻辑运算符中的运算表，写成代码，并检查结果是否与表中一致
# | A     | B     | A and B | A or B | not A |
# | ----- | ----- | ------- | ------ | ----- |
# | true  | false | false   | true   | false |
# | false | true  | false   | true   | true  |
# | false | false | false   | false  | true  |
# | true  | true  | true    | true   | false |
# 例如：
a = True
b = False
print(a and b)
print(a or b)
print(not a)

# 2. 华氏温度转换为摄氏温度
# 英语国家大多采用华氏温度，华氏温度转设置温度转换公式为C=(F-32)*5/9，摄氏温度转华氏温度公式为：F=(C*9/5)+32
# 编写一个程序将用户输入的华氏温度抓换为摄氏温度，输入如下：
# 请输入华氏温度：68
# 摄氏温度为：20

tem = int(input('请输入华氏温度:'))
tem1 = (tem-32)*5/9
print(f'华氏温度:{tem}转换为摄氏温度是:{tem1}')
