# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 02:12
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c4s6_其它运算符.py
# @Software : PyCharm

# 成员运算符
a = 3
b = (1, 2, 3, 4, 5)
print(a in b)

# 身份运算符
a = 20
b = 20
print(a is b)
