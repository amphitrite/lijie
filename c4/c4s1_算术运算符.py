# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 11:47
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c4s1_算术运算符.py
# @Software : PyCharm

print(5 / 2)  # 除尽
print(5 // 2)  # 整除
print(5 % 2)  # 求余
print(5 ** 2)  # n次幂
