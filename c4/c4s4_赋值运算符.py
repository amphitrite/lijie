# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 01:54
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c4s4_赋值运算符.py
# @Software : PyCharm

a = 1
b = 2
c = a + b

b += a * b  # b = b+a*b
print(b)
