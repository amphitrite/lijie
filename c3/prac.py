# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/13 下午 02:09
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1. 画图说明以下代码的执行的内存指向和b的值是多少，并说明为什么
a = 10
b = a
a = 100

#

# 2.将以下数据转换为其它三种类型，并观察结果
# 整数
a = 1
# 浮点数
b = 1.1
# 布尔值
c = True
d = False
# 字符串
e = None
f = '123'
g = "abc"

print(float(a))
print(bool(a))
print(str(b))
print(int(b))
print(bool(b))
print(str(b))
print(float(c))
print(bool(c))
print(str(c))
print(float(a))
print(bool(a))
print(str(a))
print(float(a))
print(bool(a))
print(str(a))

# 3.使用input函数，输出类似"我是xxx，我今天xx岁"
# name = input('请输入名字:')
# age = int(input('请输入年龄：'))
# print('我的名字是：%s,我的年龄是：%3d岁' % (name, age))
# print(f'我的名字是：{name},我的年龄是：{age:3d}岁')

# 4.获取多个用户输入的名字、年龄、身高和体重，连成一句话打印出来。要求：用几种不同的字符串格式化方式打印，并且要求对齐。
name1 = input('请输入名字:')
age1 = int(input('请输入年龄：'))
height1 = float(input('请输入身高：'))
weight1 = float(input('请输入体重：'))
name2 = input('请输入名字:')
age2 = int(input('请输入年龄：'))
height2 = float(input('请输入身高：'))
weight2 = float(input('请输入体重：'))
print('我是：%5s,我今年%3d岁，身高%4.2f米，体重%5.1f公斤。' % (name1, age1, height1, weight1))
print('我是：%5s,我今年%3d岁，身高%4.2f米，体重%5.1f公斤。' % (name2, age2, height2, weight2))
print(f'我是：{name1:5},我今年{age1:3}岁,身高{height1:4.2f},体重{weight1:5.1f}。')
print(f'我是：{name2:5},我今年{age2:3}岁,身高{height2:4.2f},体重{weight2:5.1f}。')
