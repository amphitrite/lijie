# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 10:05
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c3s4_基本数据类型.py
# @Software : PyCharm

# 1.整数
a = 1
print(type(a))

# 2.浮点数
a = 1.1
print(type(a))

# 3.布尔值
a = True
print(type(a))

# 4.字符串
b = 'a'
c = "123"
d = '''abc
123
'''
print(type(b))
print(type(c))
print(type(d))

# str1 = "he said:"hello!""
str1 = "he said:'hello!'"

#转义字符
print('\\')
print('\'')
print('\"')
print('\n')
print('\t')