# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 10:43
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c3s5_数据类型的转换.py
# @Software : PyCharm

# 错误示例
age = 18
print('my age is:' + str(age))

# str():将其它数据类型转换为字符串
a = 10
b = 10.1
c = True
print(str(a))
print(str(b))
print(str(c))

# int()：将其它数据类型转换为整数，不可以转换含有非数字的字符串
a = 10.1
b = True
c = '100'
d = 'a100'
print(int(a))
print(int(b))
print(int(c))
# print(int(d)) #不可以转换含有非数字的字符串

# float():将其它数据类型转换为浮点数，不可以转换含有非数字的字符串
a = 10
b = True
c = '100.1'
d = 'a100.1'
print(float(a))
print(float(b))
print(float(c))
# print(float(d)) #不可以转换含有非数字的字符串

# bool():将其它数据类型转换为布尔类型
a = -10
b = 10.1
c = 'abc'
print(bool(a))
print(bool(b))
print(bool(c))
