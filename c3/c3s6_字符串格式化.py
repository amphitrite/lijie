# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 11:02
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c3s6_字符串格式化.py
# @Software : PyCharm

# 1.占位符格式化字符串
name = '汇智动力'
age = 7
print("我的名字是：%s，我的年龄是：%d" % (name, age))
# 指定宽度
print("我的名字是：%s，我的年龄是：%10d" % (name, age))
# 指定宽度，高位补零
print("我的名字是：%s，我的年龄是：%010d" % (name, age))

pi = 3.1415926
# 指定宽度
print('圆周率是:%10f' % (pi))
# 指定宽度和小数位数
print('圆周率是:%10.2f' % (pi))
# 左对齐
print('圆周率是:%-10.2f' % (pi))

print('---------------------------------------------')
# 2.f函数格式化字符串
name = '汇智动力'
age = 7
print(f'我的名字是：{name},我的年龄是：{age}')

pi = 3.14159265
# 指定宽度
print(f'圆周率的值是：{pi:10f}')
# 指定宽度和小数位数
print(f'圆周率的值是：{pi:10.2f}')
# 补零
print(f'圆周率的值是：{pi:010.2f}')
# 左对齐
print(f'圆周率的值是：{pi:<010.2f}')
