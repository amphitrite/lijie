# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/30 下午 05:10
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c3s1_标识符.py
# @Software : PyCharm

# 1. 所有标识符可以包括英文、数字以及下划线（_），但不能以数字开头
# 2. 标识符区分大小写，大小写敏感
# 3. 关键字不能用作标识符
# 4. 以下划线开头的标识符是有特殊意义的。
#   1. 以单下划线开头 _foo 的代表不能直接访问的类属性，需通过类提供的接口进行访问，不能
#       用"from xxx import *"而导入
#   2. 以双下划线开头的 __foo 代表类的私有成员；以双下划线开头和结尾的 __foo__ 代表python
#       里特殊方法专用的标识，通常有特殊含义，尽量避免这种写法。如 __init__() 代表类的构造函数