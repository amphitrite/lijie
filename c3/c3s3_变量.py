# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 上午 09:50
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c3s3_变量.py
# @Software : PyCharm

# 变量可以简单理解为一个容器
# 实际上变量存储的是数据的内存地址
a = 10
print(type(a))
a = 'abc'
print(type(a))

a = 10
print(id(a))
a = 100
print(id(a))

a = 10
b = a
print(id(a))
print(id(b))