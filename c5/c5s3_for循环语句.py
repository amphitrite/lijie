# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 04:33
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c5s3_for循环语句.py
# @Software : PyCharm

# 1.range()函数:产生一个序列
print(list(range(10)))
print(list(range(0, 10)))
print(list(range(0, 10, 2)))

# 1-100累加
sum1 = 0
for i in range(1, 101):
    sum1 = sum1 + i
print(sum1)

# 5!=5x4x3x2x1
sum1 = 1
for i in range(1, 11):
    sum1 = sum1 * i
print(sum1)

# 2. 打印出以下图形
# *
# *
# *
# *
# *
for i in range(5):
    print('*')

# 3. 打印出以下图形
# * * * * *
for i in range(10):
    print('*', end=' ')

print('\n-----------------------------')
for i in range(5):
    for j in range(5):
        print('*', end=' ')
    print()

# 1. 如何打印如下图形
# *
# * *
# * * *
# * * * *
# * * * * *
print('-----------------------------')
for i in range(5):
    for j in range(i + 1):
        print('*', end=' ')
    print()

# 2. 打印出以下9x9乘法表
# 1x1=1
# 1x2=2 2x2=4
# 1x3=3 2x3=6 3x3=9
# ....
# 1x9=9 ... 9x9=81
print('------------------')
for i in range(1, 10):
    for j in range(1, i + 1):
        print(f'{i}x{j}={i * j:2}', end=' ')
    print()

# break和continue
print('-------------------------')
# break:终止整个循环
for i in range(1, 6):
    print(f'第{i}次循环，break之前')
    if i == 3:
        break
    print(f'第{i}次循环，break之后')

print('-------------------------')
# continue:提前结束本次循环，开启下一次
for i in range(1, 6):
    print(f'第{i}次循环，continue之前')
    if i == 3:
        continue
    print(f'第{i}次循环，continue之后')
