# _*_ coding: UTF-8 _*_
# @Time     : 2020/10/13 上午 10:36
# @Author   : Li Jie
# @Site     : http://www.cdtest.cn/
# @File     : prac1.py
# @Software : PyCharm

# 1、闰年问题（输入一个年份，判断是否为闰年）
# 能被4整除 不能被100整除
# 或者能被400整除
# 4年一闰，百年不闰，400年又闰
# year = int(input('输入一个年份：'))
# if year % 4 == 0:
#     if year % 100 == 0:
#         if year % 400 == 0:
#             print(f'{year}是闰年')
#         else:
#             print(f'{year}不是闰年')
#     else:
#         print(f'{year}是闰年')
# else:
#     print(f'{year}不是闰年')
#
# if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
#     print(f'{year}是闰年')
# else:
#     print(f'{year}不是闰年')

# 2、小明身高1.75，体重80kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
# （1）低于18.5：过轻
# （2）18.5-25：正常
# （3）25-28：过重
# （4）28-32：肥胖
# （5）高于32：严重肥胖
height = 1.75
weight = 68
bmi = weight / height ** 2
print(bmi)
if bmi < 18.5:
    print('过轻')
elif 18.5 <= bmi < 25:
    print('正常')
elif 25 <= bmi < 28:
    print('微胖')
elif 28 <= bmi < 32:
    print('胖')
elif bmi > 32:
    print('严重肥胖')
else:
    pass

# 3.猜数字：系统给出一个1-10之间的整数，用户输入猜测的数字，系统给出相应的提示
# import random
#
# num = random.randint(1, 10)
# print(num)
# n = int(input('请输入一个数字(1-10):'))
# if n == num:
#     print('对了')
# elif n > num:
#     print('大了')
# else:
#     print('小了')

# 4.打印以下图形
# * * * * *
# * * * *
# * * *
# * *
# *
print('---------------------------------------')
for i in range(5):
    for j in range(5 - i):
        print('*', end=' ')
    print()

# 5.打印以下图形
# * * * * *
#   * * * *
#     * * *
#       * *
#         *
print('---------------------------------------')
for i in range(5):
    for j in range(i):
        print(' ', end=' ')
    for k in range(5 - i):
        print('*', end=' ')
    print()

# 6.打印以下图形
#         *
#       * *
#     * * *
#   * * * *
# * * * * *
print('---------------------------------------')
for i in range(5):
    for j in range(5 - i - 1):
        print(' ', end=' ')
    for k in range(i + 1):
        print('*', end=' ')
    print()

# 7.打印以下图形
#       *
#     * * *
#   * * * * *
# * * * * * * *
#   * * * * *
#     * * *
#       *
print('---------------------------------------')
for i in range(4):
    for j in range(4 - i - 1):
        print(' ', end=' ')
    for k in range(2 * i + 1):
        print('*', end=' ')
    print()
for i in range(3):
    for j in range(i + 1):
        print(' ', end=' ')
    for k in range(2 * 3 - 2 * i - 1):
        print('*', end=' ')
    print()

print('--------------------------------')
num = 51
for i in range(num):
    # 空格打印
    if i <= num // 2 - 1:
        for j in range(num // 2 - i):
            print(' ', end=' ')
    elif i > (num // 2 - 1):
        for j in range(i - num // 2):
            print(' ', end=' ')

    # *号打印
    if i <= num // 2:
        for k in range(2 * i + 1):
            print('*', end=' ')
    elif i > num // 2:
        for k in range(num - 2 * (i - num // 2)):
            print('*', end=' ')
    print()
