# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 03:56
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c5s2_while循环语句.py
# @Software : PyCharm

# 1-100的累加
a = 1  # 初始化语句
sum1 = 0
while a <= 100:  # 循环条件
    sum1 += a  # 循环体
    a += 1  # 迭代语句
print(sum1)

# 用while循环实现1-10的累乘,1x2x3x4x...x10=?
a = 1
sum2 = 1
while a <= 10:
    sum2 *= a
    a += 1
print(sum2)
