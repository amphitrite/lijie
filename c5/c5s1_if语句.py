# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/1 下午 02:45
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c5s1_if语句.py
# @Software : PyCharm

# 1.基本if语句
money = 0
if money >= 5:
    print('吃豆浆和包子')
print('去上课')

# 用户输入年龄，请大于25岁的站到右边去！
# age = int(input('请输入年龄：'))
# if age > 25:
#     print('请站在右边')

# 2.if-else语句
# age = int(input('请输入年龄：'))
# if age > 25:
#     print('请站在右边')
# else:
#     print('请站在左边')

# 用户输入一个数，判断这个数是偶数还是奇数？将结果打印出来。
# num = int(input('输入一个数：'))
# if num % 2 == 0:
#     print(f'{num}是一个偶数')
# else:
#     print(f'{num}是一个奇数')

# 3.if-elif-else语句
score = 59
if score >= 90:
    print('优秀')
elif score >= 80:
    print('良好')
elif score >= 60:
    print('合格')
else:
    print('不合格')

# 课堂练习
# 根据输入月份判断春夏秋冬季节
month = int(input('请输入月份:'))
if month in (3, 4, 5):
    print('春天')
elif month in (6, 7, 8):
    print('夏天')
elif month in (9, 10, 11):
    print('秋天')
elif month in (12, 1, 2):
    print('冬天')
else:
    print(f'{month}不是一个合法月份')

# 4.if语句嵌套
hasMelon = True
isFresh = True
if hasMelon:
    if isFresh:
        print('买两个番茄和一个西瓜')
    else:
        print('买两个番茄和一个哈密瓜')
else:
    print('买两个番茄')
