# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/10 上午 11:13
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w2s1_元素的基本操作.py
# @Software : PyCharm

from selenium import webdriver
import time

from selenium.webdriver.common.by import By


def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # 模拟键盘输入，只能针对<input>
    driver.find_element(By.ID, 'kw').send_keys('圣诞节')
    time.sleep(2)

    # 模拟鼠标左键单击，针对所有标签
    driver.find_element(By.ID, 'su').click()
    time.sleep(2)

    # 清空输入框：最好每次输入前先清空，只针对<input>
    driver.find_element(By.ID, 'kw').clear()

    driver.find_element(By.ID, 'kw').send_keys('元旦')
    time.sleep(2)

    # 获取元素文本内容
    text = driver.find_element(By.XPATH, '//*[@id="2"]/div/div/h3/a').text
    print(text)

    # 获取元素的属性值
    att = driver.find_element(By.XPATH, '//*[@id="2"]/div/div/h3/a').get_attribute('href')
    print(att)

    driver.quit()


if __name__ == "__main__":
    foo1()
