# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/10 下午 02:00
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w2s2_进阶操作.py
# @Software : PyCharm

from selenium import webdriver
import time
from selenium.webdriver.common.by import By

# 下拉菜单操作
# 针对<select>标签写的下拉菜单

from selenium.webdriver.support.select import Select  # 导入Select下拉菜单类


def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("D:\pyworkspace\hz_t3\web2\example.html")
    time.sleep(1)

    # 1.直接点击选项
    driver.find_element(By.XPATH, '//*[@id="Selector"]/option[2]').click()
    time.sleep(2)

    # 2.通过生成Select下拉菜单的对象来操作下拉菜单
    element = driver.find_element(By.ID, 'Selector')  # 找到下拉菜单元素
    select = Select(element)  # 构建Select类对象

    # 通过option的value属性值选择
    select.select_by_value('banana')  # 选香蕉
    time.sleep(2)

    # 通过option的text文本选择
    select.select_by_visible_text("4.桔子")
    time.sleep(2)

    # 通过option的index值：从0开始
    select.select_by_index(4)  # 选择葡萄
    time.sleep(2)

    driver.quit()


# 文件上传
def foo2():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("D:\pyworkspace\hz_t3\web2\example.html")
    time.sleep(1)

    # 直接对上传按键<input>标签sendkeys，把文件路径输入
    driver.find_element(By.NAME, 'attach[]').send_keys(r'C:\Users\LiJie\Pictures\1.jpg')
    time.sleep(2)

    driver.quit()


# 弹出框操作
def foo3():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"D:\pyworkspace\hz_t3\web2\example.html")
    time.sleep(1)

    # 1.alert弹出框
    driver.find_element(By.NAME, 'alterbutton').click()
    time.sleep(2)
    # 切换并获取弹出框对象
    alert = driver.switch_to.alert
    # 点击弹出框“确定”
    alert.accept()
    time.sleep(2)

    # 2.confirm弹出框
    driver.find_element(By.NAME, 'confirmbutton').click()
    time.sleep(2)
    # 切换并获取弹出框对象
    confirm = driver.switch_to.alert
    # 点击取消
    confirm.dismiss()
    time.sleep(1)
    # 点击确定
    confirm.accept()
    time.sleep(2)

    # 3.prompt弹出框
    driver.find_element(By.NAME, 'promptbutton').click()
    time.sleep(2)
    # 切换并获取弹出框对象
    prompt = driver.switch_to.alert
    # 在弹出框输入
    prompt.send_keys('汇智动力')
    # 点击确定
    prompt.accept()
    time.sleep(1)
    # 再点一次确定
    prompt.accept()
    time.sleep(2)

    driver.quit()


# 单选和多选框操作
def foo4():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"D:\pyworkspace\hz_t3\web2\example.html")
    time.sleep(1)

    # 单选框：直接click，注意click有效范围
    driver.find_element(By.XPATH, '//*[@id="radio"]/input[1]').click()
    time.sleep(2)

    # 复选框：最好先判断是否已选中，再点击勾选
    element = driver.find_element(By.ID, 'web')
    element.click()
    time.sleep(1)
    if element.is_selected():  # 返回元素是否被勾选
        pass
    else:
        element.click()
    time.sleep(2)

    driver.quit()


# iframe内嵌网页操作
def foo5():
    driver = webdriver.Chrome()
    driver.maximize_window()
    # driver.get(r"D:\pyworkspace\hz_t3\web2\example.html")
    # time.sleep(1)
    #
    # # 切换到iframe标签
    # # 1.根据iframe的name或id属性，2.根据iframe的index，3.根据iframe元素
    # driver.switch_to.frame("iframe1")
    # time.sleep(2)

    # # 点击iframe内的“课程”
    # driver.find_element(By.XPATH, '//*[@id="bs-example-navbar-collapse-1"]/ul[1]/li[2]/a').click()
    # time.sleep(3)

    driver.get('https://music.163.com/#/discover/toplist')

    # 切换下层内嵌页面
    driver.switch_to.frame('g_iframe')
    # driver.switch_to.frame(driver.find_element(By.XPATH,'//*[@id="g_iframe"]'))

    # driver.switch_to.parent_frame()  # 切换上一层页面
    # driver.switch_to.default_content()  # 切换到最外层html

    driver.find_element(By.XPATH, '//span[@data-res-id="1899129849"]').click()
    time.sleep(60)

    driver.quit()


# 多窗口切换
def foo6():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"http://news.baidu.com/")

    # 循环点击6个热点新闻
    for i in range(1, 7):
        driver.find_element(By.XPATH, f'//*[@id="pane-news"]/div/ul/li[{i}]/strong/a').click()
        time.sleep(1)

    # 获取当前浏览器所有窗口的句柄
    handles = driver.window_handles
    print(handles)

    # 窗口切换
    for handle in handles:
        driver.switch_to.window(handle)  # 根据窗口的句柄切换
        if driver.current_url == "https://xhpfmapi.xinhuaxmt.com/vh512/share/10449628":  # 判断页面url
            break  # 停止切换
        else:
            driver.close()
        time.sleep(1)

    time.sleep(3)
    driver.quit()


if __name__ == "__main__":
    # foo1()
    # foo2()
    # foo3()
    # foo4()
    # foo5()
    foo6()
