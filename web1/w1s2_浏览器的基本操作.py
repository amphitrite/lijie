# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/9 下午 03:10
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w1s2_浏览器的基本操作.py
# @Software : PyCharm

from selenium import webdriver
import time


def foo1():
    # 打开浏览器，生成浏览器对象WebDiver
    driver = webdriver.Chrome()

    # 设置浏览器窗口大小
    driver.set_window_size(800, 600)
    time.sleep(1)

    # 浏览器最大化
    driver.maximize_window()
    # driver.minimize_window()#最小化浏览器为任务栏图标

    # 打开网址
    driver.get("https://www.baidu.com/")
    time.sleep(1)

    # 浏览器后退
    driver.back()
    time.sleep(1)

    # 浏览器前进
    driver.forward()
    time.sleep(1)

    # 刷新浏览器
    driver.refresh()
    time.sleep(3)

    # 获取网页title
    title = driver.title
    print(f'网页标题:{title}')

    # 获取网页url
    url = driver.current_url
    print(f'网页url:{url}')

    # 关闭浏览器
    driver.quit()  # 关闭所有的浏览器的窗口
    # driver.close()  # 关闭当前的浏览器窗口


if __name__ == "__main__":
    foo1()
