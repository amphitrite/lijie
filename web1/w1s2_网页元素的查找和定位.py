# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/9 下午 04:39
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w1s2_网页元素的查找和定位.py
# @Software : PyCharm

from selenium import webdriver
import time
from selenium.webdriver.common.by import By


# 网页元素的查找


def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # 元素WebElement的查找
    element = driver.find_element_by_id("kw")
    element = driver.find_element(By.ID, "kw")  # selenium4.0开始推荐这种写法
    time.sleep(1)

    # 输入框输入
    element.send_keys("圣诞节礼物")
    time.sleep(3)

    driver.quit()


# 元素的定位方式：
"""
1.id：id属性
2.name：name属性
3.link text：链接文本
4.partial link text：部分链接文本
5.class name：class属性
6.tag name：标签名
7.css selector：css选择器
8.xpath：xpath定位
"""


# 1.id定位
def foo2():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # 元素WebElement的查找
    # element = driver.find_element_by_id("kw")
    # id属性值定位：最推荐的定位方式，一般id唯一而且id定位最快
    driver.find_element(By.ID, "kw").send_keys('圣诞节')  # selenium4.0开始推荐这种写法
    time.sleep(3)

    driver.quit()


# 2.name定位
def foo3():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # name定位
    driver.find_element(By.NAME, "wd").send_keys('圣诞节')
    time.sleep(3)

    driver.quit()


# 3.link text：只针对<a>链接标签，通过链接文本定位
def foo4():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # link text
    # driver.find_element_by_link_text("新闻").click()
    driver.find_element(By.LINK_TEXT, "新闻").click()  # click:模拟鼠标左键单击
    time.sleep(3)

    driver.quit()


# 4.partial link text:只针对<a>链接标签，通过部分链接文本定位
def foo5():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # partial link text
    # driver.find_element_by_link_text("新闻").click()
    driver.find_element(By.PARTIAL_LINK_TEXT, "hao").click()
    time.sleep(3)

    driver.quit()


# 5.class name定位:通过class属性值定位，一般用于定位多个元素
def foo6():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # class name
    list1 = driver.find_elements(By.CLASS_NAME, "mnav")  # 获取所有class属性为mnav的元素列表
    for element in list1:  # 循环所有的元素列表
        print(element.text)  # 打印元素的文本
    time.sleep(3)

    driver.quit()


# 6. tag name：通过标签名定位，一般不用于定位单个元素
def foo7():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # tag name
    list1 = driver.find_elements(By.TAG_NAME, "a")  # 获取所有的<a>链接标签
    for element in list1:
        print(element.text)  # 打印元素的文本

    time.sleep(3)

    driver.quit()


# 7.css selector：通过css选择器定位
def foo8():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # css selector
    driver.find_element(By.CSS_SELECTOR, "#s-top-left > a:nth-child(3)").click()  # 点击“地图”

    time.sleep(3)

    driver.quit()


# 8.xpath：通过xpath路径定位元素，最强大的定位方式也是最慢的
def foo9():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")

    # css selector
    driver.find_element(By.XPATH, '//*[@id="s-top-left"]/a[4]').click()  # 点击“地图”

    time.sleep(3)

    driver.quit()


if __name__ == "__main__":
    # foo1()
    # foo2()
    # foo3()
    # foo4()
    # foo5()
    # foo6()
    # foo7()
    # foo8()
    foo9()