# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/30 下午 02:59
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c2s1_基本输入输出.py
# @Software : PyCharm

# 基本输出
print('hello world')
print('你好，汇智动力')

# 打印不换行
print('hello world', end='+')
print('你好，汇智动力')

print('a', 'b', 'c')  # 打印多个值
print(3 * 8)  # 打印表达式
print('*' * 5)

print('----------------------我是分割线------------------------')
# 基本输入
name = input('您的名字：')
print(name)
