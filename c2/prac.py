# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/30 下午 04:00
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : prac.py
# @Software : PyCharm

# 1. 使用一个print或多个print函数打印输出一首诗：

# 人生若只如初见，
# 何事秋风悲画扇。
# 等闲变却故人心，
# 却道故人心易变。

# 2. 使用print和input函数编写如下输出：
