# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/30 下午 03:39
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c2s2_注释.py
# @Software : PyCharm

# 注释
# 1.单行注释

# 单独注释一行
print('#单行注释')  # 放在行末

# 2.多行注释
'''
行1
行2
'''

"""
行1
行2
"""

# 3.声明注释
# _*_ coding: UTF-8 _*_
# 声明文件编码
