# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/3 上午 10:54
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c7s1_字符串String.py
# @Software : PyCharm

# 字符串的序列操作
# 索引操作
str1 = '你好，汇智动力！'
print(str1[0])
for i in str1:
    print(i, end=' ')

# 切片操作
print()
print(str1[1:5])
print(str1[1:])
print(str1[:5])
print(str1[::2])

# 字符串相加
str1 = 'abc'
str2 = '123'
str3 = str1 + str2
print(str3)

# 字符串的分割与拼接
# 分割
str1 = '汇智动力官网: http://www.hzdledu.com'
list1 = str1.split()  # 根据空字符：空格，换行符，\t制表符
print(list1)
list2 = str1.split('.')
print(list2)
list3 = str1.split('/')
print(list3)
# 拼接
str1 = ' '.join(list1)
print(str1)
str2 = '.'.join(list2)
print(str2)
str3 = '/'.join(list3)
print(str3)

# 字符串的检索
str1 = '汇智动力官网: http://www.hzdledu.com'
print(str1.count(":"))  # 计算包含字符串的个数
print(str1.count(":", 0, 6))  # 包左不包右
print(str1.find("www"))  # 返回字符串首字母的index，不存在返回-1
print(str1.index('www'))  # 返回字符串首字母的index，不存在抛出异常错误

# 去除字符串两边的特殊字符
str1 = '...http://www.hzdledu.com...'
print(str1.lstrip('.'))
print(str1.rstrip('.'))
print(str1.strip('.'))
