# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/19 上午 11:10
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1.获取字符串 '11:12:11> 001 enter chatroom, level2'中的用户id和等级
str1 = '11:12:11> 001 enter chatroom, level2'
print(str1[10:13])
print(str1[30:37])
list1 = str1.split()
print(list1[1])
print(list1[4])

print('----------------------------------------------------------')
# 2.获取下面字符串中的所有用户id和等级
# '''11:12:11> 001 enter chatroom, level2
# 11:12:11> 22 enter chatroom, level3
# 11:12:11> 0004 enter chatroom, level105
# 11:12:11> 003 enter chatroom, level99'''
str1 = '''11:12:11> 001 enter chatroom, level2
11:12:11> 22 enter chatroom, level3
11:12:11> 0004 enter chatroom, level105
11:12:11> 003 enter chatroom, level99'''
str1 = str1.split('\n')
print(str1)
for line in str1:
    line = line.split()
    print(line[1])
    print(line[4])

print('----------------------------------------------------------')
# 3.将字符串'abcd[0910efg&*(]hijkl[ada2r4545]mn03opq$st'中的非字母字符去掉
import re

str1 = 'abcd[0910efg&*(]hijkl[ada2r4545]mn03opq$st'

# 使用正则sub替换
pattern = r'[^a-zA-Z]'
str2 = re.sub(pattern, '', str1)
print(str2)

# 使用字符串的序列特点
str2 = ""
for i in str1:
    if i.isalpha():
        str2 = str2 + i
print(str2)

print('----------------------------------------------------------')

print('----------------------------------------------------------')
# 5.编写一个程序，要求用户输入一个字符串，将字符串中重复的字符串去除，如输入'abcaadef'，输出为'abcdef'
str1 = 'abcaadef'

str2 = ""
for i in str1:
    if i not in str2:
        str2 += i
print(str2)