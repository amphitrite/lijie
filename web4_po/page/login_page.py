# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/14 下午 02:48
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : login_page.py
# @Software : PyCharm
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 显示等待类
from selenium.webdriver.support import expected_conditions  # 等待条件
import unittest


class LoginPage:
    """
    登录页面类
    定义所有登录页面元素以及相关操作
    """

    def __init__(self, driver):
        """
        定义所有页面元素，完成页面类对象的属性初始化
        """
        self.user_input_locator = (By.XPATH, '//*[@id="account"]/input')  # 账号
        self.pwd_input_locator = (By.XPATH, '//*[@id="password"]/input')  # 密码
        self.auth_input_locator = (By.XPATH, '//*[@id="authCode"]/div/input')  # 验证码
        self.login_button_locator = (By.XPATH, '//*[@id="root"]/div/div[1]/form/div[5]/div/div/span/button')  # 登录

        self.driver = driver  # 从用例类传入driver
        # self.driver = webdriver.Chrome()  # 调试和编写使用，之后需要注释掉
        self.wait = WebDriverWait(self.driver, 10)  # 生成显示等待对象

    def input_user(self, user):
        """
        输入账号
        :param user: 账号
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.user_input_locator))
            self.driver.find_element(*self.user_input_locator).clear()
            self.driver.find_element(*self.user_input_locator).send_keys(user)
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/input_user_error.png')
            raise Exception('账号输入错误')

    def input_pwd(self, pwd):
        """
        输入密码
        :param pwd: 密码
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.pwd_input_locator))
            self.driver.find_element(*self.pwd_input_locator).clear()
            self.driver.find_element(*self.pwd_input_locator).send_keys(pwd)
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/input_pwd_error.png')
            raise Exception('密码输入错误')

    def input_auth_code(self, auth="8888"):
        """
        输入验证码
        :param auth: 验证码，默认为万能验证码8888
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.auth_input_locator))
            self.driver.find_element(*self.auth_input_locator).clear()
            self.driver.find_element(*self.auth_input_locator).send_keys(auth)
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/input_auth_error.png')
            raise Exception('验证码输入错误')

    def click_login(self):
        """
        点击登录按键
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.login_button_locator))
            self.driver.find_element(*self.login_button_locator).click()
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/click_login_error.png')
            raise Exception('点击登录错误')


if __name__ == "__main__":
    driver = webdriver.Chrome()
    login_page = LoginPage(driver)
    driver.get('http://192.168.5.92/cloud/#/open/login')
    login_page.input_user('admin')
    login_page.input_pwd('123456')
    login_page.input_auth_code()
    login_page.click_login()
    time.sleep(3)
    driver.quit()
