# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/14 下午 02:48
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : main_page.py
# @Software : PyCharm

import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 显示等待类
from selenium.webdriver.support import expected_conditions  # 等待条件
from selenium.webdriver.common.action_chains import ActionChains  # 导入动作链条类
import unittest


class MainPage:
    """
    主页面类，封装所有的主页面元素及相关操作
    """

    def __init__(self, driver):
        """
        定义所有页面元素，完成页面类对象的属性初始化
        """
        self.user_icon_locator = (By.XPATH, '//*[@id="root"]/section/section/header/div/div[2]')  # 用户头像
        self.logout_button_locator = (By.XPATH, '/html/body/div[2]/div/div/ul/li[3]')  # 退出登录

        self.driver = driver  # 从用例类传入driver
        # self.driver = webdriver.Chrome()  # 调试和编写使用，之后需要注释掉
        self.wait = WebDriverWait(self.driver, 10)  # 生成显示等待对象

    def show_logout(self):
        """
        移动鼠标到用户头像
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.user_icon_locator))
            ActionChains(self.driver).move_to_element(self.driver.find_element(*self.user_icon_locator)).perform()
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/move_to_user_icon_error.png')
            raise Exception('用户头像错误')

    def click_logout(self):
        """
        点击退出登录按键
        :return: None
        """
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(self.logout_button_locator))
            self.driver.find_element(*self.logout_button_locator).click()
        except Exception as e:
            self.driver.get_screenshot_as_file('./log/logout_button_error.png')
            raise Exception('退出登录按键错误')
