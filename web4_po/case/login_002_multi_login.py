# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/14 下午 04:54
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : login_002_multi_login.py
# @Software : PyCharm

import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 显示等待类
from selenium.webdriver.support import expected_conditions  # 等待条件
import unittest
from web4_po.page.login_page import LoginPage  # 导入页面层的页面类
from web4_po.page.main_page import MainPage
from web4_po.common.read_file import read_csv


class Login001(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get('http://192.168.5.92/cloud/#/open/login')

        # 实例化页面类的对象
        self.login_page = LoginPage(self.driver)
        self.main_page = MainPage(self.driver)

        # 读取用户账号
        self.users = read_csv('./data/user.csv')

    def test(self):
        for user in self.users:
            self.login_page.input_user(user[0])  # 输入账号
            self.login_page.input_pwd(user[1])  # 输入密码
            self.login_page.input_auth_code()  # 输入验证码
            self.login_page.click_login()  # 点击登录
            self.main_page.show_logout()  # 显示退出菜单
            self.main_page.click_logout()  # 点击退出登录
        # time.sleep(3)

    def tearDown(self) -> None:
        self.driver.quit()
