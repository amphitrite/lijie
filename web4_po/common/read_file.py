# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/3 下午 04:20
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : read_file.py
# @Software : PyCharm

# 读取csv文件
def read_csv(filename):
    with open(filename,mode='r',encoding='utf8') as file1:
        users = file1.readlines()
        users_new = []
        for user in users:
            user = user.rstrip('\n').split(',')
            users_new.append(user)
        print(users_new)
    return users_new


if __name__=="__main__":
    read_csv('../data/user.csv')