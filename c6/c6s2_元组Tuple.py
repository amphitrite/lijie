# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/2 下午 02:59
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c6s2_元组Tuple.py
# @Software : PyCharm

# 1.元组的定义
tuple1 = (1, 1.1, False, '汇智动力')
print(tuple1[0])
print(tuple1[2])

# 2.操作元组的函数
tuple1 = (-99, 0, 99)
print(len(tuple1))
print(max(tuple1))
print(min(tuple1))

# 3.元组不能增，删，改
# tuple1[0] = 1

# 4.元组的查询
# 直接遍历元组的元素
tuple1 = (-99, 0, 99)
for i in tuple1:
    print(i)

# 通过产生index的序列遍历元组
for i in range(len(tuple1)):
    print(tuple1[i])
