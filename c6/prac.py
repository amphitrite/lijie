# _*_ coding: UTF-8 _*_
# @Time     : 2020/10/14 下午 03:00
# @Author   : Li Jie
# @Site     : http://www.cdtest.cn/
# @File     : prac1.py
# @Software : PyCharm

import random

# 1. 解决千年虫问题，以前的电脑为了节省存储空间，通常年份都是用的两位数字，这样跨过2000年后，
# 2000年和1900都会被表示为00，造成计算机系统故障，编写程序将以下序列中1970后的年份转换为4位，并升序输出。
# 当前序列:[89,98,00,75,68,37,58,90]
# 输出序列:[1937,1958,1968,1975,1989,1990,1998,2000]
list1 = [89, 98, 00, 75, 68, 37, 58, 90]
list2 = []
for i in list1:
    if i == 00:
        i += 2000
    else:
        i += 1900
    list2.append(i)
list2.sort()
print(list2)

print('----------------------------------------------')
# 2. 练习：将列表[45,23,2,5,3,2,6,45,43,21,66,2,3,2]进行从小到大排序，不能用sort()函数，
# 1.冒泡排序：通过对相邻的两个数比较大小，较大(较小)数和后一个数交换位置，实现列表的整体有序
list1 = [45, 23, 2, 5, 3, 2, 6, 45, 43, 21, 66, 2, 3, 2]
# 升序
for i in range(len(list1) - 1):
    for j in range(len(list1) - i - 1):
        if list1[j] > list1[j + 1]:
            list1[j], list1[j + 1] = list1[j + 1], list1[j]
print(list1)

list2 = []
for i in range(len(list1)):
    temp = min(list1)
    list2.append(temp)
    list1.remove(temp)
print(list2)

print('----------------------------------------------')
# 3、求 100-200以内同时能被7、8整除的数
list1 = []
for i in range(100, 201):
    if i % 7 == 0 and i % 8 == 0:
        list1.append(i)
print(list1)

# 4.找出一个列表中，只出现了一次的数字，并且保持原来的次序，例如[1,2,1,3,2,5]结果为[3,5]
print('\n----------------------------------------------')
list1 = [1, 2, 1, 3, 2, 5]
list2 = []
for i in list1:
    if list1.count(i) == 1:
        list2.append(i)
print(list2)

# 提升
# 5、求 0 -1 + 2 - 3 + 4 - 5 + 6 -7.... + 100
print('----------------------------------------------')
sum1 = 0
for i in range(101):
    if i % 2 == 0:
        sum1 += i
    else:
        sum1 -= i
print(sum1)

# 6.求100以内的素数：>1整数，只能被1和自己整除
print('----------------------------------------------')
list1 = []
for i in range(2, 101):
    count = 0
    for j in range(1, i + 1):
        if i % j == 0:
            count += 1
    if count == 2:
        list1.append(i)
print(list1)

list1 = []
for i in range(2, 101):
    isPrime = True
    for j in range(2, i):
        if i % j == 0:
            isPrime = False
            break
    if isPrime:
        list1.append(i)
print(list1)

# 7. 水仙花数：水仙花数是指一个 n 位数 ( n 大于等于 3 )，它的每个位上的数字的 n 次幂之和等于它本身。
# # （例如：1的3次方 + 5的三次方 + 3三次方 = 153）。根据这个要求，打印所有三位数的水仙花数。
print('----------------------------------------------')
list1 = []
for i in range(100, 1000):
    a = i // 100
    b = (i - a * 100) // 10
    c = i % 10
    if a ** 3 + b ** 3 + c ** 3 == i:
        list1.append(i)
print(list1)

list1 = []
for a in range(1, 10):
    for b in range(10):
        for c in range(10):
            if a ** 3 + b ** 3 + c ** 3 == a * 100 + b * 10 + c:
                list1.append(a * 100 + b * 10 + c)
print(list1)

list1 = []
for i in range(100, 1000):
    i = str(i)  # '153'
    a = int(i[0])
    b = int(i[1])
    c = int(i[2])
    if a ** 3 + b ** 3 + c ** 3 == int(i):
        list1.append(int(i))
print(list1)

# 8.一球从100米高度自由落下，每次落地后反跳回原高度的一半，再落下。
# 求它在 第10次落地时，共经过多少米？第10次反弹多高？
print('----------------------------------------------')
height = 100
sum1 = height
for i in range(9):
    height = height / 2
    sum1 += 2 * height
print(f'第10次落地经过：{sum1}米，第10次反弹高度为：{height / 2}米')

# 9. 随机产生20个100-200之间的正整数存放到列表中，
# 并求列表中的所有元素最大值、最小值、平均值，然后将各元素的与平均值的差组成一个列表
print('----------------------------------------------')
# 产生随机数列表
list4 = []
for i in range(20):
    num = random.randint(100, 200)
    list4.append(num)
print(list4)

max1 = max(list4)
min1 = min(list4)
sum1 = sum(list4)
avg1 = sum1 / len(list4)
print(f'最大值是:{max1}，最小值是:{min1},平均值是:{avg1}')
list2 = []
for i in list4:
    list2.append(i - avg1)
print(list2)
