# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/2 上午 10:55
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c6s1_列表List.py
# @Software : PyCharm

# 1.列表的定义
list1 = [1, 1.1, True, '汇智动力']
list2 = []  # 空列表
print(list1[0])
print(list1[3])

# 2.操作列表的函数
# len()获取列表长度
print(len(list1))

# max()获取列表元素最大值，所有元素为数值
list1 = [-99, 0, 99]
print(max(list1))

# min()获取列表元素最小值，所有元素为数值
print(min(list1))

# 3.增加列表元素
list1 = [-99, 0, 99]
list1.append(100)  # 添加元素到末尾
print(list1)
list1.insert(1, 2)  # 指定index位置插入
print(list1)

# 4.删除列表元素
list1 = [-99, 0, 99, 100]
list1.pop()  # 默认删除最后一个
print(list1)
list1.pop(0)  # 删除指定index的元素
print(list1)

# 5.修改列表元素
list1 = [-99, 0, 99, 100]
list1[1] = 50  # 对指定index重新赋值
print(list1)

# 6.查询列表
# 直接遍历列表
list1 = [-99, 0, 99, 100]
for i in list1:
    print(i)
    i += 1
print(list1)

# 通过产生index的序列遍历列表
for i in range(len(list1)):
    print(list1[i])
    list1[i] += 1
print(list1)

# 排序算法
print('--------------------------------------------------')
list1 = [5, 2, 3, 4, 1]
for i in range(len(list1) - 1):  # 外层循环决定比较次数
    for j in range(len(list1) - i - 1):  # 内存循环决定每次比较哪些数
        if list1[j] > list1[j + 1]:  # 如果前一个数较大
            list1[j], list1[j + 1] = list1[j + 1], list1[j]  # 交换值
print(list1)
