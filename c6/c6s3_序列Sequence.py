# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/2 下午 03:13
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c6s3_序列Sequence.py
# @Software : PyCharm

# 序列包括：list列表、tuple元祖、string字符串
# 1.序列的索引操作:index可以正的整数值也可以是负的整数值（倒数第n个）
list1 = [1, 2, 3]
tuple1 = (1, 2, 3)
str1 = 'abc'
print(list1[-1])
print(tuple1[-1])
print(str1[-1])

# 2.序列中包含序列
list1 = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
# 使用两层index访问，左侧是高维或外层index，右侧是低维或内层index
print(list1[0][0])
print(list1[1][0])
print(list1[2][0])

# 遍历使用两层循环
for i in list1:
    for j in i:
        print(j, end=',')
    print()

# 3.序列的切片
list1 = [1, 2, 3, 4, 5, 6]
print(list1[1:2])
print(list1[:2])
print(list1[1:])
print(list1[::2])
print(list1[-6:-3])

# 4.序列的相加
list1 = [1,2,3]
list2 = [4,5,6]
list3 = list1+list2
print(list3)

