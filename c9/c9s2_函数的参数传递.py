# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/3 下午 04:02
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c9s2_函数的参数传递.py
# @Software : PyCharm

# 1.必须参数:按照个数和顺序传递实参给形参
def foo1(a, b):
    print(f'a={a},b={b}')


foo1(1, 2)


# foo1(1)
# foo1(1,2,3)


# 2.关键字参数：实参中用赋值语句给指定形参赋值
def foo2(a, b):
    print(f'a={a},b={b}')


foo2(b=1, a=2)


# 3.默认值参数：形参中为形参设置默认值
def foo3(a, b=10):
    print(f'a={a},b={b}')


foo3(1, 2)
foo3(1)
