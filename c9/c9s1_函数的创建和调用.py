# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/3 下午 03:17
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c9s1_函数的创建和调用.py
# @Software : PyCharm

# 1.创建函数
def add(a, b):  # a,b:形参
    c = a + b  # 函数的代码块
    return c  # 结束函数调用，将值返回


sum1 = add(1, 99)  # 1,99:实参
print(sum1)


# 编写一个函数，必须参数a,b,c，函数计算a的平方、b的立方、c三者的和，并返回。
def foo1(a, b, c):
    return a ** 2 + b ** 3 + c


print(foo1(1, 2, 3))
