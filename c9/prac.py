# _*_ coding: UTF-8 _*_
# @Time     : 2020/10/15 下午 05:00
# @Author   : Li Jie
# @Site     : http://www.cdtest.cn/
# @File     : prac1.py
# @Software : PyCharm

# 1.# 求 1-100以内所有含6的数
def foo1():
    list1 = []
    for i in range(1, 101):
        a = i // 10
        b = i % 10
        if a == 6 or b == 6:
            list1.append(i)
    print(list1)

    list2 = []
    for i in range(1, 101):
        i = str(i)
        if "6" in i:
            list2.append(int(i))
    print(list2)


# 2、Chuckie Lucky赢了100W美元，
# 他把它存入一个每年盈利8%的账户。
# 在每年的最后一天，Chuckie取出10W美元。
# 编写一个程序，计算需要多少年Chuckie就会清空他的账户。
def foo2():
    money = 100
    years = 0
    while money >= 10:
        money *= 1.08
        money -= 10
        years += 1
    print(f'还剩{money}万元,已经取了{years}年')


# 3、题目：猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个 第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
# 以后每天早上都吃了前一天剩下的一半零一个。到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。
# 程序分析：采取逆向思维的方法，从后往前推断。
def foo3():
    nums = 1
    for i in range(9):
        nums = 2 * (nums + 1)
    print(nums)


# 4、不使用自带函数，写一个函数，用于返回一个数的绝对值
import math


def foo4(n):
    if n >= 0:
        return n
    else:
        return -1 * n


if __name__ == '__main__':
    # foo1()
    # foo2()
    # foo3()
    print(foo4(-9))
    print(foo4(9))
