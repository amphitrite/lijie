# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/6 上午 10:56
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c9s3_函数的局部与全局变量.py
# @Software : PyCharm

"""
函数的局部变量：定义在函数内，在函数外无法访问
"""


def foo1():
    num = 6
    print(f'函数内访问局部变量:{num}')


foo1()
# print(f'函数外访问局部变量:{num}'

"""
全局变量:定义在函数外，在函数内和函数外都可以访问
"""
num1 = 100


def foo2():
    print(f'函数内访问全局变量:{num1}')


foo2()
print(f'函数外访问全局变量:{num1}')

"""
global:将函数内的变量提升为全局变量
"""
def foo1():
    global num2
    num2 = 6
    print(f'函数内访问global全局变量:{num2}')


foo1()
print(f'函数外访问global全局变量:{num2}')
