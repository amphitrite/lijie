# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/6 上午 11:14
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c9s4_函数的返回值.py
# @Software : PyCharm

"""
函数的返回值：可以理解为函数的值，函数中如果不写return语句，那么会自动添加一个return None在最后
"""


def foo1(a, b):
    return a + b


def foo2(a, b):
    c = a + b


print(foo1(1, 2))
print(foo2(1, 2))
