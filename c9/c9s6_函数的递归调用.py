# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/6 上午 11:33
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c9s6_函数的递归调用.py
# @Software : PyCharm

"""
函数的递归：函数自己调用自己，将一个问题不断变小，直到有一个确定答案
"""


# 递归实现1+2+3+4+...+100
def foo1(n):
    if n == 1:  # 停止递归的条件
        return 1
    else:  # 继续递归
        return n + foo1(n - 1)  # n个数的累加=n+n-1个数的累加


if __name__ == '__main__':
    print(foo1(100))

