# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/14 上午 11:22
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : jd_test.py
# @Software : PyCharm

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 显示等待类
from selenium.webdriver.support import expected_conditions  # 等待条件
import unittest


class JD_Test(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver,30)

    def test(self):
        self.driver.get('http://news.baidu.com/')

        self.wait.until(
            expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="pane-news"]/div/ul/li[1]/strong/a')))
        text = self.driver.find_element(By.XPATH, '//*[@id="pane-news"]/div/ul/li[1]/strong/a').text  # 获取第一条新闻的text
        # 预期与实际结果的验证
        self.assertEqual("唱响奋进凯歌 凝聚民族力量1", text)

    def tearDown(self) -> None:
        self.driver.quit()
