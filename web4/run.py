# _*_ coding: UTF-8 _*_
# @Time     : 2021/11/3 上午 10:59
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : run.py
# @Software : PyCharm

import unittest
import HTMLTestRunner

# 将测试用例加入测试套件suite，批量运行，以及生成测试报告

# 1.将测试用例加入测试套件
# 扫描文件夹，将用例加入测试套件(只能扫描继承了unittest.TestCase的用例)
suite = unittest.defaultTestLoader.discover('./', '*.py')

# 2.生成运行器，指定测试报告
file = open('./test_result.html', mode='wb')
runner = HTMLTestRunner.HTMLTestRunner(file, title='测试报告', description='汇智动力', tester='李杰')

# 3.运行测试套件
runner.run(suite)

# 4.关闭文件
file.close()
