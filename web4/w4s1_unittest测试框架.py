# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/14 上午 10:35
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : w4s1_unittest测试框架.py
# @Software : PyCharm

# unittest测试框架的作用（unittest,pytest）
# 1.管理测试用例，批量化运行
# 2.参数化用例
# 3.统计用例执行结果，提供测试报告
import unittest  # 导入测试框架模块


class Test(unittest.TestCase):  # 定义一个类继承unittest.TestCase
    # 前置条件
    def setUp(self) -> None:
        print('setUp')

    # 测试步骤:方法必须以test开头
    def test(self):
        print('test')

    # 后置处理：清理测试环境
    def tearDown(self) -> None:
        print('tearDown')
