# _*_ coding: UTF-8 _*_
# @Time     : 2021/1/22 下午 01:50
# @Author   : Li Jie
# @Site     : http://www.hzdledu.com/
# @File     : prac1.py
# @Software : PyCharm

# 1. 有score1.txt，有以下成绩数据：
# 张三 99 0 59
# 李四 80 75 59
# 王麻子 100 10 10
# 使用文件的读写操作，计算每个同学的总分，并写入score2.txt
# 张三 99 0 59 158
# 李四 80 75 59 214
# 王麻子 100 10 10 120
def foo1():
    # 1.读取数据
    with open('./score1.txt', mode='r', encoding='utf') as file:
        list1 = file.readlines()
    print(list1)

    # 2.处理数据
    list2 = []
    for line in list1:
        line = line.split()  # 分割每行
        print(line)
        sum = int(line[1]) + int(line[2]) + int(line[3])  # 计算总分
        print(sum)
        line.append(str(sum))  # 添加总分到每行列表
        print(line)
        line = " ".join(line) + "\n"  # 拼接每行数据
        print(line)
        list2.append(line)  # 将处理后的每行加入新列表
    print(list2)

    # 3.写入数据
    with open('./score2.txt', mode='w', encoding='utf8') as file:
        file.writelines(list2)


# 3.现有文件salary1.txt， 记录了公司员工的薪资，其内容格式如下
#
# name: Jack   ;    salary:  12000
#  name :Mike ; salary:  12300
# name: Luk ;   salary:  10030
#   name :Tim ;  salary:   9000
# name: John ;    salary:  12000
# name: Lisa ;    salary:   11000
#
# 每个员工一行，记录了员工的姓名和薪资，
# 每行记录 原始文件中并不对齐，中间有或多或少的空格
#
# 现要求实现一个python程序，计算出所有员工的税后工资（薪资的90%）和扣税明细，
# 以如下格式存入新的文件salary2.txt中，如下所示
#
# name: Jack   ;    salary:  12000 ;  tax: 1200 ; income:  10800
# name: Mike   ;    salary:  12300 ;  tax: 1230 ; income:  11070
# name: Luk    ;    salary:  10030 ;  tax: 1003 ; income:   9027
# name: Tim    ;    salary:   9000 ;  tax:  900 ; income:   8100
# name: John   ;    salary:  12000 ;  tax: 1200 ; income:  10800
# name: Lisa   ;    salary:  11000 ;  tax: 1100 ; income:   9900
#
# 要求对齐，上面看着如果不齐，应该是字体原因
# tax 表示扣税金额和 income表示实际收入。注意扣税金额和 实际收入要取整数
def foo2():
    # 1.读取数据
    with open('./salary.txt', 'r', encoding='utf8') as file:
        list1 = file.readlines()
    print(list1)

    # 2.计算数据
    list2 = []
    for line in list1:
        line = line.rstrip('\n').split(';')
        print(line)
        line[0] = line[0].split(":")
        print(line[0])
        line[1] = line[1].split(":")
        print(line[1])
        name = line[0][1].strip()  # 提取姓名
        salary = line[1][1].strip()  # 提取工资
        print(name, salary)
        tax = int(salary) * 0.1  # 计算所得税
        salary_taxed = int(salary) * 0.9  # 税后工资
        print(name, salary, tax, salary_taxed)

        line = f"name: {name:5}   ;    salary:  {salary:>7} ;  tax: {tax:6.0f} ; income:  {salary_taxed:7.0f}\n"
        list2.append(line)
    print(list2)
    # 3.写入文件
    with open('./salary2.txt', mode='w', encoding='utf8') as file:
        file.writelines(list2)


# 3、附件中存在三个txt文件，teacher.txt存的是老师的id和老师的名字，course.txt存的是课程Id和课程名称，teacher_course.txt存的是课程id和老师Id
# # 的对应关系
# # 题目要求，根据teacher_course.txt表将老师的名字和课程的名称对应在一起，存到新的文件里，格式如下：
#
# teacher.txt

# id;user_id;desc;display_idx;realname
# 42;106;;1;小猪老师
# 43;107;;2;mandy老师
# 44;108;;3;jessie老师
# 45;109;;4;小阳老师
# 46;110;;5;小布老师
# 47;111;;6;江老师
# 48;210;;7;心田老师
# 49;211;;8;文立老师
# 50;212;;9;菜菜老师
# 51;213;;10;刘莉莉老师

# # teacher_course.txt

# teacher_id;course_id
# 42;32
# 42;33
# 42;34
# 42;35
# 42;36
# 42;37
# 42;38
# 42;39
# 42;40
# 43;32
# 43;33
# 43;34
# 43;35
# 43;36
# 43;37
# 43;38
# 43;39
# 43;40
# 44;32
# 44;33
# 44;34
# 44;35
# 44;36
# 44;37
# 44;38
# 44;39
# 44;40
# 45;32
# 45;33
# 45;34
# 45;35
# 45;36
# 45;37
# 45;38
# 45;39
# 45;40
# 46;32
# 46;33
# 46;34
# 46;35
# 46;36
# 46;37
# 46;38
# 46;39
# 46;40
# 48;32
# 48;33
# 48;34
# 48;35
# 48;36
# 48;37
# 48;38
# 48;39
# 48;40
# 49;32
# 49;33
# 49;34
# 49;35
# 49;36
# 49;37
# 49;38
# 49;39
# 49;40
# 50;32
# 50;33
# 50;34
# 50;35
# 50;36
# 50;37
# 50;38
# 50;39
# 50;40
# 51;32
# 51;33
# 51;34
# 51;35
# 51;36
# 51;37
# 51;38
# 51;39
# 51;40

# course.txt

# id;name;desc;display_idx
# 32;软件测试框架课程;;1
# 33;web测试技术课程;;2
# 34;app测试技术课程;;3
# 35;数据库课程;;4
# 36;linux系统课程;;5
# 37;NET+课程;;6
# 38;QTP自动化课程;;7
# 39;LR性能测试课程;;8
# 40;JAVA程序设计课程;;9

# 输出为以下格式：
# teacher_name;course_name
# 小猪老师;软件测试框架课程
# 小猪老师;web测试技术课程
# 小猪老师;app测试技术课程
# 小猪老师;数据库课程t
# 小猪老师;linux系统课程
# 小猪老师;NET+课程
# 小猪老师;QTP自动化课程
# 小猪老师;LR性能测试课程
# 小猪老师;JAVA程序设计课程
# mandy老师;软件测试框架课程

def foo3():
    # 1.读取三个文件的数据
    with open('./teacher_course.txt', mode='r', encoding='utf8') as file:
        teacher_courses = file.readlines()
    with open('./teacher.txt', mode='r', encoding='utf8') as file:
        teachers = file.readlines()
    with open('./course.txt', mode='r', encoding='utf8') as file:
        courses = file.readlines()
    # print(teacher_courses)
    # print(teachers)
    # print(courses)

    # 处理数据
    #将三个文件数据初步处理
    teachers_new = ['teacher_name;course_name\n']
    for line in teachers:
        line = line.strip('\n').split(';')
        teachers_new.append(line)
    print(teachers_new)

    courses_new = []
    for line in courses:
        line = line.strip('\n').split(';')
        courses_new.append(line)
    print(courses_new)

    teacher_courses_new = []
    for line in teacher_courses:
        line = line.rstrip('\n').split(";")
        teacher_courses_new.append(line)
    print(teacher_courses_new)

    #根据teacher_course的数据去其它两个列表查找
    teacher_courses = ['teacher_name;course_name\n']
    for line in teacher_courses_new:
        teacher_id = line[0]
        course_id = line[1]
        teacher_name = ""
        for teacher in teachers_new:
            if teacher[0] == teacher_id:
                teacher_name = teacher[4]
        course_name = ""
        for course in courses_new:
            if course[0]==course_id:
                course_name = course[1]
        if teacher_name!= "" and course_name!="":
            teacher_courses.append(f'{teacher_name};{course_name}\n')
    print(teacher_courses)

    #写入数据
    with open('./teacher_course_new.txt',mode='w',encoding='utf8') as file:
        file.writelines(teacher_courses)


if __name__ == '__main__':
    # foo1()
    # foo2()
    foo3()
