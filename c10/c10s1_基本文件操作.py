# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/6 下午 02:01
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c10s1_基本文件操作.py
# @Software : PyCharm

def foo1():
    # 打开文件
    file1 = open(r'D:\pyworkspace\hz_t3\c10\1.txt', mode='r', encoding='utf8')

    # 关闭文件
    file1.close()


def foo2():
    # 打开文件并自动关闭
    with open('./1.txt', mode='r', encoding='utf8') as file1:
        pass


"""
文件读取的三种方法：
1.read()
2.readline()
3.readlines()
"""


# read()读取整个文件，或指定个数的字符
def foo3():
    with open('./1.txt', mode='r', encoding='utf8') as file:
        str1 = file.read()
        print(str1)


# readline():一次读取文件的一行为字符串
def foo4():
    with open('./1.txt', mode='r', encoding='utf8') as file:
        str1 = file.readline()
        print(str1)


# readlines():将文件的所有行读为字符串列表，每行是一个字符串
def foo5():
    with open('./1.txt', mode='r', encoding='utf8') as file:
        list1 = file.readlines()
        print(list1)


"""
字符串的写入:
1.write()
2.writelines()
"""


# write():将字符串写入文件
def foo6():
    with open('./2.txt', mode='w', encoding='utf8') as file:
        str1 = "犇！\n2021\n汇智动力\n"
        file.write(str1)


# writelines():将字符串列表写入文件
def foo7():
    with open('./2.txt', mode='w', encoding='utf8') as file:
        list1 = [
            '骉！\n',
            '2022\n',
            '汇智动力\n'
        ]
        file.writelines(list1)


if __name__ == '__main__':
    # foo1()
    # foo2()
    # foo3()
    # foo4()
    # foo5()
    # foo6()
    foo7()
