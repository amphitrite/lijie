# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/8 下午 03:05
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c14s2_异常.py
# @Software : PyCharm

# 异常Exception
# 在代码运行过程可能出现的错误，中断程序的执行
def foo1():
    for i in range(-2, 3):
        print(5 / i)
        print('除法之后的语句')


def foo2():
    list1 = [1, 2, 3]
    print(list1[3])


if __name__ == '__main__':
    # foo1()
    foo2()
