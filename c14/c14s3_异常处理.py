# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/8 下午 03:15
# @Author   : Li Jie
# @Site     : http://www.hzdledu.cn/
# @File     : c14s3_异常处理.py
# @Software : PyCharm

"""
异常处理：
1.try-except
2.try-except-else
3.try-except-else-finally
"""


# 1.try-except
def foo1():
    for i in range(-2, 3):
        try:
            print(5 / i)  # 可能出现异常的语句
        except Exception as e:  # 指明捕获的异常类型
            print(e)  # 出现异常之后的处理
        print('除法之后的语句')


# 2.try-except-else
def foo2():
    for i in range(-2, 3):
        try:
            print(5 / i)  # 可能出现异常的语句
        except Exception as e:  # 指明捕获的异常类型
            print(e)  # 出现异常之后的处理
        else:
            print('没有异常')
        print('除法之后的语句')
        print('------------------')


# 3. try-except-else-finally
def foo3():
    for i in range(-2, 3):
        try:
            print(5 / i)  # 可能出现异常的语句
        except Exception as e:  # 指明捕获的异常类型
            print(e)  # 出现异常之后的处理
        else:
            print('没有异常')
        finally:
            print('finally总会被执行')
        print('除法之后的语句')
        print('------------------')


if __name__ == "__main__":
    # foo1()
    # foo2()
    foo3()